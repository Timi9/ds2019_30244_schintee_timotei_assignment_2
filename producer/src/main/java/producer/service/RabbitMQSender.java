package producer.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import producer.model.Activity;

@Service
public class RabbitMQSender {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${javainuse.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${javainuse.rabbitmq.routingkey}")
	private String routingkey;	

	
	public void send(Activity activity) {
		amqpTemplate.convertAndSend(exchange, routingkey, activity);
		System.out.println("[Send activity] : " + activity.getActivityName() + " "+ activity.getStartTime() + " "+ activity.getEndTime());
	    
	}
}